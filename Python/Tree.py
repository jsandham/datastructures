from Stack import Stack

class Node():
	def __init__(self, val):
		self.data = val
		self.left = None
		self.right = None



class Tree():
	def __init__(self):
		self.root = None

	def iterative_insert(self, val):
		prev = None
		curr = self.root

		while curr != None:
			prev = curr

			if val <= curr.data:
				curr = curr.left
			else:
				curr = curr.right

		if prev == None:
			self.root = Node(val)
		else:
			if val <= prev.data:
				prev.left = Node(val)
			else:
				prev.right = Node(val)

	def recursive_insert(self, val):
		if self.root == None:
			self.root = Node(val)
		else:
			self.insert(self.root, val)

	def insert(self, node, val):
		if val < node.data:
			if node.left != None:
				self.insert(node.left, val)
			else:
				node.left = Node(val)
		else:
			if node.right != None:
				self.insert(node.right, val)
			else:
				node.right = Node(val)

		# if node == None:
		# 	node = Node(val)
		# else:
		# 	if val <= node.data:
		# 		self.insert(node.left, val)
		# 	else:
		# 		self.insert(node.right, val)

	def iterative_inorder_traversal(self, array):
		stack = Stack()
		curr = self.root
		done = False

		while done == False:
			if curr != None:
				stack.push(curr)
				curr = curr.left
			else:
				if stack.isEmpty() == False:
					curr = stack.pop()
					array.append(curr.data)
					curr = curr.right
				else:
					done = True
		return array

	def recursive_inorder_traversal(self, array):
		pass

	def inorder_traversal(self):
		pass

	def iterative_delete(self):  # not actually needed, this is Python after all!!
		stack = Stack()
		stack.push(self.root)

		while stack.isEmpty() == False:
			curr = stack.pop()
			if curr != None:
				stack.push(curr.left)
				stack.push(curr.right)
				curr = None

	def recursive_delete(self):  # not actually needed, this is Python after all!!
		self.delete(self.root)

	def delete(self, node):
		if node != None:
			self.delete(node.left)
			self.delete(node.right)
			node = None








mytree = Tree()

# mytree.iterative_insert(5)
# mytree.iterative_insert(7)
# mytree.iterative_insert(3)
# mytree.iterative_insert(4)
# mytree.iterative_insert(9)

mytree.recursive_insert(5)
mytree.recursive_insert(7)
mytree.recursive_insert(3)
mytree.recursive_insert(4)
mytree.recursive_insert(9)



print mytree.iterative_inorder_traversal([])



# class test:
# 	def __init__(self):
# 		self.n = None

# 	def increment(self, m):
# 		m = 1
# 		print self.n

# 	def answer(self):
# 		self.increment(self.n)
# 		print self.n

# t = test()
# t.answer()
# print t.n
