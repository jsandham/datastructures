class Stack():
	def __init__(self):
		self.size = 10
		self.top = 0
		self.stack = [None]*self.size

	def push(self, data):
		if self.top >= self.size:
			self.stack = self.stack + [None]*10
		self.stack[self.top] = data
		self.top += 1

	def pop(self):
		if self.top >= 0:
			self.top -= 1
			return self.stack[self.top]
		else:
			return None

	def isEmpty(self):
		if self.top <= 0:
			return True
		else:
			return False