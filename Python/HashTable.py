class HashTable():
	def __init__(self):
		self.size = 11
		self.keys = [None] * self.size
		self.values = [None] * self.size

	def hashfunction(self, key, size):
		return key%size

	def rehash(self, oldhash, size):
		return (oldhash+1)%size

	def __getitem__(self, key):
		h = self.hashfunction(key, len(self.keys))

		start = h
		value = None
		found = False
		stop = False
		while self.keys[h] != None and found == False and stop == False:
			if self.keys[h] == key:
				found = True
				value = self.values[h]
			else:
				h = self.rehash(h, len(self.keys))
				if h == start:
					stop = True
		return value

		

	def __setitem__(self, key, value):
		h = self.hashfunction(key, len(self.keys))

		if self.keys[h] == None:
			self.keys[h] = key
			self.values[h] = value
		else:
			if self.keys[h] == key:
				self.values[h] = value
			else:
				while self.keys[h] != None:
					h = self.rehash(h, len(self.keys))

				self.keys[h] = key
				self.values[h] = value



table = HashTable()
table[45] = 'james'
table[23] = 'merry'
table[59] = 'daniel'
table[61] = 'david'
table[75] = 'adam'

print table.keys
print table.values

print table[23]