#ifndef __TREE_H__
#define __TREE_H__

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>


typedef struct Node
{
  int value;
  Node *left;
  Node *right;
}Node;

void iterative_insert(int val, Node **ptr);
void recursive_insert(int val, Node **ptr);
void delete_tree(Node *ptr);
void sort(Node *ptr, int *array, int *index);


#endif
