#ifndef __STACK_H__
#define __STACK_H__

	#include <stdio.h>
	#include <stdlib.h>

	typedef int Type;

	typedef struct Stack
	{
	  Type *data;
	  int top;
	}Stack;


	void init(Stack **stack, int size);
	void delete_stack(Stack **stack);
	void push(Stack **stack, Type e);
	Type pop(Stack stack);
	int stack_size(Stack stack);


#endif
