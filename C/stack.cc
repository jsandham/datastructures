#include "stack.h"


void init(Stack **stack, int size)
{
  *stack = (Stack*)malloc(sizeof(Stack));

  (*stack)->top = -1;
  (*stack)->data = (Type*)malloc(sizeof(Type)*size);
}


void delete_stack(Stack **stack)
{
  free((*stack)->data);
  free((*stack));
}


void push(Stack **stack, Type e)
{
  (*stack)->top += 1;
  (*stack)->data[(*stack)->top] = e;
}


Type pop(Stack stack)
{
  stack.top -= 1;
  return stack.data[stack.top+1];
}


int stack_size(Stack stack)
{
  return stack.top+1;
}


int main()
{
  Stack *my_stack;
  init(&my_stack, 10);

  push(&my_stack, 3);
  push(&my_stack, 5);
  push(&my_stack, 7);

  printf("%d\n", pop(*my_stack));
  printf("%d\n", stack_size(*my_stack));

  delete_stack(&my_stack);
}

