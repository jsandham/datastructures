#ifndef __LINKED_LIST_H__
#define __LINKED_LIST_H__

	#include <stdlib.h>
	#include <stdio.h>

	typedef int Type;

	typedef struct Node
	{
	  Type data;
	  Node *next;
	}Node;


	void init(Node **list, Type data);
	void delete_list(Node **list);
	void print_list(Node **list);
	void insert(Node **list, Type data);
	Node find(Type data);


#endif

