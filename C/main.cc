#include <stdlib.h>
#include <stdio.h>

enum sibling
{
  james, 
  merry
};



int sqr(int n)
{
  return n*n;
}

int main(int argc, char **argv)
{
  int (*func)(int);

  func = &sqr;

  printf("%d\n", (*func)(4));


  sibling name = james;

  if(name == merry){
    printf("%d\n", (*func)(2));
  }
  else if(name == james){
    printf("%d\n", (*func)(3));
  }
}
