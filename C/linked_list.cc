#include "linked_list.h"


void init(Node **list, Type data)
{
  *list = (Node*)malloc(sizeof(Node));
  (*list)->next = NULL;
  (*list)->data = data;
}



void delete_list(Node **list)
{
  if(*list != NULL){
    delete_list(&(*list)->next);
    free(*list);
  }
}


void insert(Node **list, Type data)
{
  Node *prev = NULL;
  Node *current = *list;
  while(current != NULL){
    prev = current;
    current = current->next;
  }

  current = (Node*)malloc(sizeof(Node));
  current->data = data;
  current->next = NULL;
  prev->next = current;
}


Node find(Type data)
{
  
}


void print_list(Node **list)
{
  Node *current = *list;
  while(current != NULL){
    printf("%d\n", current->data);
    current = current->next;
  }
}



int main()
{
  Node *my_list;
  init(&my_list, 34);

  insert(&my_list, 5);
  insert(&my_list, 22);
  insert(&my_list, 43);
  print_list(&my_list);

  delete_list(&my_list);
}

