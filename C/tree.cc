#include "tree.h"


void iterative_insert(int val, Node **root)
{
  Node *newNode = (Node*)malloc(sizeof(Node));
  newNode->value = val;
  newNode->left = NULL;
  newNode->right = NULL;


  Node *prev = NULL;
  Node *current = *root;
  while(current != NULL){
    prev = current;

    if(val < current->value){
      current = current->left;
    }
    else{
      current = current->right;
    }
  }


  if(prev == NULL){
    *root = newNode;
  }
  else{
    if(val < prev->value){
      prev->left = newNode;
    }
    else{
      prev->right = newNode;
    }
  }
}


void recursive_insert(int val, Node **ptr)
{
  if(*ptr == NULL){
    *ptr = (Node*)malloc(sizeof(Node));
    (*ptr)->value = val;
    (*ptr)->left = NULL;
    (*ptr)->right = NULL;
  }
  else{
    if(val < (*ptr)->value){
      recursive_insert(val, &(*ptr)->left);
    }
    else{
      recursive_insert(val, &(*ptr)->right);
    }
  }
}


void delete_tree(Node *ptr)
{
  if(ptr != NULL){
    delete_tree((*ptr).left);
    delete_tree((*ptr).right);
    free(ptr);
  }
}


void sort(Node *ptr, int *array, int *index)
{
  if(ptr == NULL){
    
  }
  else{
    sort(ptr->left, array, index);
    array[*index] = ptr->value;
    *index = *index + 1;
    //printf("%d\n", *index);
    //printf("%d\n", ptr->value);
    sort(ptr->right, array, index);
  }  

}






int main(int argc, char **argv)
{
  // create input data array
  int *data = (int*)malloc(sizeof(int)*(argc-1));
  for(int i=1;i<argc;i++){
    data[i-1] = atoi(argv[i]);
  }

  // root node in tree
  Node *root = NULL; 

  // build tree
  for(int i=0;i<argc-1;i++){
    iterative_insert(data[i], &root);
    //insert(data[i], &root);
  }

  // sort array
  int index = 0;
  sort(root, data, &index);


  for(int i=0;i<argc-1;i++){
    printf("%d\n", data[i]);
  }

  //delete tree
  delete_tree(root);  
}

