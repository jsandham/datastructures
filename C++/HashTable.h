//********************************************************************************
//*
//*  C++ Hash Table
//*  James sandham
//*  23 August 2016
//*
//********************************************************************************
#ifndef __HASHTABLE_H__
#define __HASHTABLE_H__

#include <iostream>
#include <vector>


template <class T>
class HashTable
{
	private:
		int tableSize;
		std::vector<int> keys;
		std::vector<T> values;

		int hashfunction(int key);
		int rehash(int key);
		void resize(int size);

	public:
		HashTable();
		~HashTable();

		T get(int key);
		void set(int key, T val);
};


template <class T>
HashTable<T>::HashTable()
{
	tableSize = 11;
	keys.resize(tableSize, -1);
	values.resize(tableSize);
}

template <class T>
HashTable<T>::~HashTable()
{

}


template <class T>
T HashTable<T>::get(int key)
{
	int hash = hashfunction(key);

	int start = hash;
	bool found = false;
	if(keys[hash] == key){
		return values[hash];
	}
	else{
		while(found == false){
			hash = rehash(hash);
			if(keys[hash] == key){
				return values[hash];
			}
			else if(hash == start){
				return NULL;
			}
		}	
	}
}


template <class T>
void HashTable<T>::set(int key, T val)
{
	int hash = hashfunction(key);
	if(keys[hash] == -1){
		keys[hash] = key;
		values[hash] = val;
	}
	else{
		if(keys[hash] == key){
			values[hash] = val;
		}
		else{
			while(keys[hash] != -1){
				hash = rehash(hash);
			}
			keys[hash] = key;
			values[hash] = val;
		}
	}
}


template <class T>
int HashTable<T>::hashfunction(int key)
{
	return key % tableSize;
}


template <class T>
int HashTable<T>::rehash(int key)
{
	return (key + 1) % tableSize;
}


template <class T>
void HashTable<T>::resize(int size)
{
	tableSize = size;
	keys.resize(tableSize);
	values.resize(tableSize);
}

#endif