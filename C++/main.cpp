#include <iostream>
#include "Tree.h"
#include "HashTable.h"


int main()
{
	// Tree<int> *mytree = new Tree<int>();
	// mytree->iterative_insert(4);
	// mytree->iterative_insert(7);
	// mytree->iterative_insert(5);
	// mytree->iterative_insert(9);
	// mytree->iterative_insert(2);
	// mytree->iterative_inorder_traversal();
	// mytree->iterative_delete();

	// mytree->recursive_insert(4);
	// mytree->recursive_insert(7);
	// mytree->recursive_insert(5);
	// mytree->recursive_insert(9);
	// mytree->recursive_insert(2);
	// mytree->iterative_inorder_traversal();
	// mytree->recursive_delete();

	Tree<int> firstTree;
	firstTree.iterative_insert(4);
	firstTree.iterative_insert(7);
	firstTree.iterative_insert(5);
	firstTree.iterative_insert(9);
	firstTree.iterative_insert(2);
	firstTree.iterative_inorder_traversal();
	Tree<int> secondTree(firstTree);
	secondTree.iterative_inorder_traversal();
	Tree<int> thirdTree = secondTree;
	thirdTree.iterative_inorder_traversal();

	// mytree->recursive_delete();
	// delete mytree;








	// HashTable<int> *table = new HashTable<int>();
	// table->set(54,1);
	// table->set(77,2);
	// table->set(61,3);
	// table->set(93,4);
	// table->set(93,5);

	// std::cout<<table->get(77)<<std::endl;
	// std::cout<<table->get(54)<<std::endl;
	// std::cout<<table->get(45)<<std::endl;
	// std::cout<<table->get(93)<<std::endl;

	// delete table;
}
