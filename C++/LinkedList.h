#ifndef __LINKEDLIST_H__
#define __LINKEDLIST_H__

template <class T>
class Node
{
	public:
		T data;
		Node *next;

		Node(T data) : data(data)
		{
		}
}


template <class T>
class Linkedlist
{
	public:
		Node *head;

		Linkedlist() : head(NULL)
		{
		}


};

#endif