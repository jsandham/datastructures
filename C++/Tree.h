//********************************************************************************
//*
//*  C++ Tree
//*  James sandham
//*  13 August 2016
//*
//********************************************************************************
#ifndef __TREE_H__
#define __TREE_H__

#include <iostream>
#include "Stack.h"



template <class T>
class Node
{
	public:
		T data;
		Node<T> *left;
		Node<T> *right;

		Node(T data) : data(data), left(NULL), right(NULL)
		{
		}
};



template <class T>
class Tree
{
	private:
		Node<T> *root;

	public:
		Tree() : root(NULL)
		{
			std::cout<<"constructor called"<<std::endl;
		}
		Tree(const Tree<T> &tree);
		Tree<T>& operator=(const Tree<T> &tree);
		~Tree();

		void iterative_insert(T data);
		void recursive_insert(T data);
		void iterative_delete();
		void recursive_delete();
		void iterative_preorder_traversal();
		void iterative_inorder_traversal();
		void iterative_postorder_traversal();

	private:
		void recursive_insert_tree(T data, Node<T> **node);
		void recursive_delete_tree(Node<T> **node);
};





//********************************************************************************
//
//  tree implementation
//
//********************************************************************************

template <class T>
Tree<T>::Tree(const Tree<T> &tree)
{
	root = NULL;
	Stack<Node<T>* > stack;
	Node<T> *current = tree.root;
	bool done = false;
	while(!done){
		if(current != NULL){
			iterative_insert(current->data);
			stack.push(current);
			current = current->left;
		}
		else{
			if(!stack.isEmpty()){
				current = stack.pop();
				current = current->right;
			}
			else{
				done = true;
			}
		}				
	}
}


template <class T>
Tree<T>& Tree<T>::operator=(const Tree<T> &tree)
{
	if(this != &tree){
		iterative_delete();

		root = NULL;
		Stack<Node<T>* > stack;
		Node<T> *current = tree.root;
		bool done = false;
		while(!done){
			if(current != NULL){
				iterative_insert(current->data);
				stack.push(current);
				current = current->left;
			}
			else{
				if(!stack.isEmpty()){
					current = stack.pop();
					current = current->right;
				}
				else{
					done = true;
				}
			}				
		}
	}

	return *this;
}


template <class T>
Tree<T>::~Tree()
{
	iterative_delete();
}



template <class T>
void Tree<T>::iterative_insert(T data)
{
	Node<T> *newNode = new Node<T>(data);
	Node<T> *prev = NULL;
	Node<T> *curr = root;

	while(curr != NULL){
		prev = curr;
		if(data <= curr->data){
			curr = curr->left;
		}
		else{
			curr = curr->right;
		}
	}

	if(prev == NULL){
		root = newNode;
	}
	else{
		if(data <= prev->data){
			prev->left = newNode;
		}
		else{
			prev->right = newNode;
		}
	}
}


template <class T>
void Tree<T>::recursive_insert(T data)
{
	recursive_insert_tree(data, &root);
}


template <class T>
void Tree<T>::iterative_delete()
{
	Stack<Node<T>* > stack;

	if(root != NULL){
		stack.push(root);
	}
	while(!stack.isEmpty()){
		Node<T> *current = stack.pop();

		if(current != NULL){
			stack.push(current->left);
			stack.push(current->right);
		}
		delete current;
	}
}


template <class T>
void Tree<T>::recursive_delete()
{
	recursive_delete_tree(&root);
}



template <class T>
void Tree<T>::iterative_preorder_traversal()
{

}


template <class T>
void Tree<T>::iterative_inorder_traversal()
{
	Stack<Node<T>* > stack;
	Node<T> *current = root;
	bool done = false;

	while(!done){
		
		if(current != NULL){
			stack.push(current);
			current = current->left;
		}
		else{
			if(!stack.isEmpty()){
				current = stack.pop();
				std::cout<<current->data<<std::endl;
				current = current->right;
			}
			else{
				done = true;
			}
		}				
	}
}


template <class T>
void Tree<T>::iterative_postorder_traversal()
{

}



template <class T>
void Tree<T>::recursive_insert_tree(T data, Node<T> **node)
{
	if(*node == NULL){
		*node = new Node<T>(data);
	}
	else{
		if(data <= (*node)->data){
			recursive_insert_tree(data, &(*node)->left);
		}
		else{
			recursive_insert_tree(data, &(*node)->right);
		}
	}
}


template <class T>
void Tree<T>::recursive_delete_tree(Node<T> **node)
{
	if(*node != NULL){
		recursive_delete_tree(&(*node)->left);
		recursive_delete_tree(&(*node)->right);
		delete *node;
	}
}


#endif