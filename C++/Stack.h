//********************************************************************************
//*
//*  C++ Stack
//*  James sandham
//*  13 August 2016
//*
//********************************************************************************
#ifndef __STACK_H__
#define __STACK_H__

#include <vector>

template<class T>
class Stack
{
  private:
  	unsigned int top;
    std::vector<T> stack;

  public:
  	Stack();
  	Stack(unsigned int init_stack_size);
  	Stack(const Stack<T> &s);
  	Stack<T>& operator=(const Stack<T> &s);
    ~Stack();


  	void push(T obj);
  	T pop();
  	bool isEmpty() const;
  	unsigned int size() const;
};






//********************************************************************************
//
//  stack implementation
//
//********************************************************************************

template<class T>
Stack<T>::Stack()
{
  top = 0;
  stack.resize(10);
}


template<class T>
Stack<T>::Stack(unsigned int init_stack_size)
{
  top = 0;
  stack.resize(init_stack_size);
}


template<class T>
Stack<T>::Stack(const Stack<T> &s)
{
  top = s.top;
  for(unsigned int i=0;i<stack.size();i++){
    stack[i] = s.stack[i];
  }
}


template<class T>
Stack<T>& Stack<T>::operator=(const Stack<T> &s)
{
  if(this != &s){
    top = s.top;
    for(unsigned int i=0;i<stack.size();i++){
      stack[i] = s.stack[i];
    }
  }

  return *this;
}


template<class T>
Stack<T>::~Stack()
{

}


template<class T>
void Stack<T>::push(T obj)
{
  if(stack.size() > top){
    stack.resize(top + 100);
  }
  stack[top] = obj;
  top++;
}


template<class T>
T Stack<T>::pop()
{
  top--;
  return stack[top];
}


template<class T>
bool Stack<T>::isEmpty() const
{
  if(top == 0){
  	return true;
  }
  else{
  	return false;
  }
}


template<class T>
unsigned int Stack<T>::size() const
{
  return top;
}

#endif