using System.Collections.Generic;

class Stack<T>
{
	int top;
	List<T> stack;

	public Stack(){
		top = 0;
		stack = new List<T>();
	}

	public void push(T data){
		stack.Add(data);
		top++;
	}

	public T pop(){
		top--;
		return stack[top];
	}


	public bool isEmpty(){
		if(top == 0){
			return true;
		}
		else{
			return false;
		}
	}

	public int size(){
		return top;
	}
}
