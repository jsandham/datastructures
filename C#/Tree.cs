using System;
using System.Collections.Generic;

class Node<T> where T:IComparable<T>
{
	public T data;
	public Node<T> left;
	public Node<T> right;

	public Node(T d){
		data = d;
		left = null;
		right = null;
	}
}



class Tree<T> where T:IComparable<T>
{
	private Node<T> root;

	public Tree(){
		root = null;
	}

	public void iterative_insert(T data){
		Node<T> newNode = new Node<T>(data);
		Node<T> curr = root;
		Node<T> prev = null;

		while(curr != null){
			prev = curr;
			/* if(data <= curr.data){ */
			if((curr.data).CompareTo(data) > 0){
				curr = curr.left;
			}
			else{
				curr = curr.right;
			}
		}

		if(prev == null){
			root = newNode;
		}
		else{
			/* if(data <= prev.data){ */
			if((prev.data).CompareTo(data) > 0){
				prev.left = newNode;
			}
			else{
				prev.right = newNode;
			}
		}
	}

	public void recursive_insert(T data){
		recursive_insert_tree(data, root);
	}

	public void iterative_inorder_traversal(){
		// Stack<Node> stack = new Stack<Node>();
		Stack<Node<T> > stack = new Stack<Node<T> >();
		Node<T> curr = root;
		bool done = false;

		while(!done){
			if(curr != null){
				stack.Push(curr);
				curr = curr.left;
			}
			else{
				if(stack.Count!=0){
					curr = stack.Pop();
					Console.WriteLine(curr.data);
					curr = curr.right;
				}
				else{
					done = true;
				}

			} 
		}
	}

	private void recursive_insert_tree(T data, Node<T> node){
		if(node != null){
			/* if(data <= node.data){ */ 
			if((node.data).CompareTo(data) > 0){
				recursive_insert_tree(data, node.left);
			}
			else{
				recursive_insert_tree(data, node.right);
			}
		}
		else{
			node = new Node<T>(data);
		}
	}
}