using System;

class MainProgram
{
	static void Main()
	{
		Tree<int> mytree = new Tree<int>();
		mytree.iterative_insert(3);
		mytree.iterative_insert(6);
		mytree.iterative_insert(2);
		mytree.iterative_insert(9);
		mytree.iterative_insert(5);
		mytree.iterative_insert(8);

		mytree.iterative_inorder_traversal();
	}
}